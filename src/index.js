const path = require('path')
const express = require('express')
const http = require('http')
const socketio = require('socket.io')
const Filter = require('bad-words')
const { generateMessage, generateLocationMessage } = require('./utils/messages')
const { addUser, removeUser, getUser, getUsesinRoom } = require('./utils/users')

const app = express()
const server = http.createServer(app)
const io = socketio(server)
const port = process.env.PORT || 3000

const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))

//const msg = 'Welcome!'
// let newMsg = ''

io.on('connection', (socket) =>
{
    console.log('New WebSocket connection')

    socket.on('join', (options, callback) => {
        const {error, user } = addUser({ id: socket.id, ...options})

        if(error){
            return callback(error)
        }

        socket.join(user.room)

        socket.emit('msgWelcome', generateMessage(user.username, 'Welcome'))
        socket.broadcast.to(user.room).emit('msgWelcome', generateMessage(user.username,`${user.username} entrou na sala de chat!`))

        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsesinRoom(user.room)
        })

        callback()
    })

   
    socket.on('sendMessage', (menssagem, callback) =>
    {
        const user = getUser(socket.id)
        // const filter = new Filter()

        // if (filter.isProfane(menssagem))
        // {
        //     alert(socket.emit('msgWelcome', generateMessage(user.username, 'Não é permitido palavras de ofensivas! ')))
        //     //return callback(socket.emit('msgWelcome', generateMessage(user.username, 'Oh meu amigo! ta achando que aqui é um cabaré? pode escrever palavrão não filhão.')))
        // }
        io.to(user.room).emit('msgWelcome', generateMessage(user.username, menssagem))
        callback()
    })
    socket.on('sendLocalization', (coordenadas, callback) =>
    {
        const user = getUser(socket.id)
        io.to(user.room).emit('locationMessage', generateLocationMessage(user.username, `https://www.google.com/maps/?q=${coordenadas.latitude},${coordenadas.longitude}`))
        callback()
    })

    socket.on('disconnect', () =>
    {
        const user = removeUser(socket.id)
        if(user) {
            io.to(user.room).emit('msgWelcome', generateMessage(user.username, `${user.username} saiu da Sala.`))
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: getUsesinRoom(user.room)
            })
        }
        
    })
   
})

server.listen(port)