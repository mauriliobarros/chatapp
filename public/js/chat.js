const socket = io()

// Elements
const $menssagemForm = document.querySelector('#cmenssagem')
const $menssagemFormInput = $menssagemForm.querySelector('input')
const $menssagemFormButton = $menssagemForm.querySelector('button')
const $localizacaoFormButton = document.querySelector('#localizacao-enviar')
const $messages = document.querySelector('#testando')

//Templates
const messageTemplate = document.querySelector('#message-template').innerHTML
const locationTemplate = document.querySelector('#location-template').innerHTML
const sidebarTemplate = document.querySelector('#sidebar-template').innerHTML

//Options
const {username, room} = Qs.parse(location.search, { ignoreQueryPrefix: true})

const autoscroll =() => {
    const $newMenssage = $messages.lastElementChild

    const newMenssageStyles = getComputedStyle($newMenssage)
    const newMessageMargin = parseInt(newMenssageStyles.marginBottom)
    const newMenssageHeight = $newMenssage.offsetHeight + newMessageMargin

    const visibleHeight = $messages.offsetHeight
    const containerHeight = $messages.scrollHeight

    const scrollOffset = $messages.scrollTop + visibleHeight

    if(containerHeight - newMenssageHeight >= scrollOffset) {
        $messages.scrollTop = $messages.scrollHeight
    }

}

socket.on('locationMessage', (location) =>
{
    console.log(location)
    const html = Mustache.render(locationTemplate,
    {
        url: location.url,
        nomeuser: location.username,
        createdAty: moment(location.createdAt).format("lll")
    })
    $messages.insertAdjacentHTML('beforeend',html)
    autoscroll()
})
socket.on('msgWelcome', (msg) =>
{
    console.log(msg)
    const html = Mustache.render(messageTemplate,
    {
        messagex: msg.text,
        nomeuser: msg.username,
        createdAtx: moment(msg.createdAt).format("HH:mm:ss")
    })
    $messages.insertAdjacentHTML('beforeend', html)
    autoscroll()
})

socket.on('roomData', ({room, users}) => {
    const html = Mustache.render(sidebarTemplate, {
        room,
        users
    })
    document.querySelector('#sidebar').innerHTML = html
})

$menssagemForm.addEventListener('submit', (e) =>
{
    e.preventDefault()
    
    $menssagemFormButton.setAttribute('disabled', 'disabled')
    //const menssagem = document.querySelector('input').value
    const menssagem = e.target.elements.textinho.value

    // console.log('dentrinho')
    socket.emit('sendMessage',menssagem, (error) =>
    {
        $menssagemFormButton.removeAttribute('disabled')
        $menssagemFormInput.value = ''
        $menssagemFormInput.focus()
        //console.log('A menssagem foi enviada para o servidor', message)
        if(error)
        {
            return console.log(error)
        }

    })
})

$localizacaoFormButton.addEventListener('click', () =>
{

    if(!navigator.geolocation)
    {
        return alert('Geolocation não é suportado pelo seu navegador')
    }
    $localizacaoFormButton.setAttribute('disabled', 'disabled')
    navigator.geolocation.getCurrentPosition((position) =>
    {
        //console.log(position)
        socket.emit('sendLocalization', 
        {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        }, () => 
        {
            $localizacaoFormButton.removeAttribute('disabled')
            console.log('Localização Compartilhada')
        })
    })
    
})

socket.emit('join', {username, room}, (error) =>{
    if(error) {
        alert(error)
        location.href = '/'
    }

})